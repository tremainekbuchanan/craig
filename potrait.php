
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>Potrait</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="bootstrap.css">
     <link rel="stylesheet" href="lightbox.css">
    <!-- Custom styles for this template 
    <link href="narrow.css" rel="stylesheet"> -->

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
               
  </head>

  <body>
      
      <div class="navbar navbar-inverse" role="navigation">
      <a class="navbar-brand" href="#">CH Photograhy</a>
          
          <ul class="nav navbar-nav pull-right">
            <li><a href="#">Home</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Portraits</a></li>
              <li><a href="#">Fashion</a></li>
              <li><a href="#">Fitnes</a></li>
              <li><a href="#">Weddings</a></li>
            </ul>
          </div>
     <!-- Carousel
    ================================================== -->
      
      
    
    <div class="container">
     
        <div class="row">
        
        <?php include 'retrieve.php' ?>
       
            
        </div>
       
        
        </div>
            
       
      <!-- Main component for a primary marketing message or call to action 
      <img src="NETT%20Kj%C3%B8laas.jpg" class="img-responsive">  -->
      

    </div> <!-- /container -->
      
      


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
     <script src="lightbox-2.6.min.js"></script>
  </body>
</html>
